package world;

enum GameState {
	Init;
	Idle;
	Waiting;
	AnimatingShuffle;
	SimulatingTurn;
	Harvesting;
	PlayingCard;
	EndOfPlayingCard;
	Drafting;
	EndOfDay;
	Gameover;
}
