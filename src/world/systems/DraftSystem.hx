package world.systems;

class DraftSystem extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);
	}

	override public function onEvent(event: hxd.Event): Bool {
		return false;
	}

	/**
		reset the system to the same state after constructor
	**/
	override public function reset() {
		super.reset();
	}

	/**
		Dispose the system data
	**/
	override public function dispose() {
		super.dispose();
	}

	/**
		update loop
	**/
	override public function update(dt: Float) {
		super.update(dt);
	}

	var draftWindow: DraftWindow;

	public function showDraft() {
		this.world.state = Drafting;
		var cards = generateNewDraft();
		final window = new DraftWindow(this.world, cards);
		window.onShow();
		window.setX(Globals.game.gameWidth, AlignCenter).setY(30);
		this.draftWindow = window;
		this.world.renderSystem.windowRenderSystem.showWindow(window);
	}

	public function draftComplete() {
		this.world.state = Idle;
		this.world.renderSystem.startDay.visible = true;
		this.draftWindow = null;
	}

	function generateNewDraft(): Array<Card> {
		final cards: Array<Card> = [];
		for (_ in 0...3) {
			final f = this.world.rules.cards.randomItem(this.worldState.r);
			final card: Card = cast f.make(this.world.nextId, this.worldState);
			cards.push(card);
		}
		return cards;
	}

	public var selectedDraftCard: Card;

	public function selectDraftCard(card: Card) {
		if (this.selectedDraftCard != null) {
			final rc: CardRenderComponent = cast selectedDraftCard.render;
			rc.deckDisplay.highlightColor = null;
		}
		this.selectedDraftCard = card;
		if (this.selectedDraftCard != null) {
			final rc: CardRenderComponent = cast selectedDraftCard.render;
			rc.deckDisplay.highlightColor = 0xffffda97;
		}
	}

	public function onDeckCardChosen(card: Card) {
		if (this.selectedDraftCard == null) return;
		swapCardWithDraft(card, this.selectedDraftCard);
	}

	function swapCardWithDraft(deckCard: Card, draftCard: Card) {
		final rc: CardRenderComponent = cast draftCard.render;
		rc.deckDisplay.highlightColor = null;

		final index = this.worldState.deck.indexOf(deckCard);
		this.world.renderSystem.swapCardWithCard(index, deckCard, draftCard);
		this.worldState.deck[index] = draftCard;
		draftWindow.close();
		draftComplete();
		this.world.renderSystem.hideCard();
	}
}
