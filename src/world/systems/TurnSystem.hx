package world.systems;

class TurnSystem extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);
	}

	override public function onEvent(e: hxd.Event) {
#if debug
		if (e.kind == EKeyDown && e.keyCode == hxd.Key.ENTER) {
			if (this.world.state != SimulatingTurn) return false;
			simulateTurn(true);
		}
		if (e.kind == EKeyDown && e.keyCode == hxd.Key.SPACE) {
			this.world.pause = !this.world.pause;
		}
#end
		return super.onEvent(e);
	}

	/**
		reset the system to the same state after constructor
	**/
	override public function reset() {
		super.reset();
	}

	/**
		Dispose the system data
	**/
	override public function dispose() {
		super.dispose();
	}

	/**
		update loop
	**/
	override public function update(dt: Float) {
		super.update(dt);
		if (this.world.state == SimulatingTurn) simulateTurn();
	}

	/**
		Start the simulation of the turn
	**/
	public function startDay() {
		this.world.renderSystem.startDay.visible = false;
		// ensure that it is game state
		if (this.world.state != Idle) return;

		// start animating the shuffle
		this.world.state = AnimatingShuffle;
		this.world.renderSystem.shuffleDeck(() -> {
			this.worldState.currentPlaying = -1;
			this.world.state = SimulatingTurn;
		});
	}

	function simulateTurn(force: Bool = false) {
		if (force == false && this.world.pause == true) return;
		// if there is no more card to play, we return back to EndOfDay
		if (this.worldState.currentPlaying == this.worldState.deck.length - 1) {
			endDay();
			return;
		}

		this.worldState.currentPlaying += 1;
		final plot: Plot = this.worldState.plots[this.worldState.currentPlot];
		this.world.state = Harvesting;
		if (plot.plot.crop != null) {
			harvestCrop(plot, plot.plot.crop, playCard);
		} else {
			this.world.updater.wait(0.5 / this.world.worldSpeed).thenRun(playCard);
		}
	}

	function playCard() {
		final card = this.worldState.deck[this.worldState.currentPlaying];
		this.world.cardSystem.playCard(card);
		final rc: CardRenderComponent = cast card.render;
		rc.deckDisplay.alpha = .5;
	}

	function endDay() {
		this.world.state = EndOfDay;
		for (card in this.worldState.deck) {
			final rc: CardRenderComponent = cast card.render;
			rc.deckDisplay.alpha = 1;
		}
		final rentalAmount = this.world.worldState.rent;
		if (rentalAmount > this.world.worldState.gold) {
			this.world.state = Gameover;
			final window = new GameoverWindow(this.world);
			window.onShow();
			window.setX(Globals.game.gameWidth, AlignCenter).setY(Globals.game.gameHeight, AlignCenter);
			this.world.renderSystem.windowRenderSystem.showWindow(window);
			return;
		}

		final window = new PayRentWindow(this.world);
		window.onShow();
		window.setX(Globals.game.gameWidth, AlignCenter).setY(Globals.game.gameHeight, AlignCenter);
		this.world.renderSystem.windowRenderSystem.showWindow(window);
	}

	public function harvestCrop(plot: Plot, crop: Crop, onFinish: Void->Void = null) {
		if (crop.state.cropStage == Matured || crop.state.cropStage == Withered) {
			final rc: PlotRenderComponent = cast plot.render;
			final icon = new h2d.Bitmap(rc.cropIcon.tile);
			var direction = 0;
			final bounds = rc.cropIcon.getBounds(this.world.renderSystem.drawLayers);
			var abortHarvest = false;
			if (crop.state.cropStage == Matured) {
				final context = new CropSystemContext(this.world, crop, plot);
				final canHarvest = crop.crop.canHarvest(context);
				if (canHarvest == true) {
					final multiplier = crop.crop.valueMultiplier;
					this.worldState.gold += crop.state.value * multiplier;
					if (crop.crop.persist == false) plot.plot.crop = null;
					direction = -1;
				} else {
					abortHarvest = true;
				}
			} else {
				plot.plot.crop = null;
				direction = 1;
			}
			if (abortHarvest == false) {
				icon.setX(bounds.xMin).setY(bounds.yMin);
				this.world.renderSystem.worldLayers.add(icon, 1000);
				this.world.updater.run(new Batch([
					new MoveByAmountByDuration(icon.wo(), [0, direction * 10], .5 / this.world.worldSpeed),
					new AlphaTo(icon.wo(), 0, 1 / (.5 / this.world.worldSpeed)),
				]).whenDone(() -> {
					if (onFinish != null) onFinish();
				}));
			} else {
				if (onFinish != null) this.world.updater.wait(0.5 / this.world.worldSpeed).thenRun(onFinish);
			}
		} else {
			if (onFinish != null) this.world.updater.wait(0.5 / this.world.worldSpeed).thenRun(onFinish);
		}
	}
}
