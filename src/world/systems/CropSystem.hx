package world.systems;

class CropSystem extends System {
	public function new() {
		super();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);
	}

	override public function onEvent(event: hxd.Event): Bool {
		return false;
	}

	/**
		reset the system to the same state after constructor
	**/
	override public function reset() {
		super.reset();
	}

	/**
		Dispose the system data
	**/
	override public function dispose() {
		super.dispose();
	}

	/**
		update loop
	**/
	override public function update(dt: Float) {
		super.update(dt);
		if (this.world.state == EndOfPlayingCard) donePlayingCard();
	}

	function donePlayingCard() {
		// grow all the crops
		for (plot in this.worldState.plots) {
			final crop = plot.plot.crop;
			if (crop == null) continue;
			trigger(plot, crop);
		}
		this.world.state = Waiting;
		this.world.updater.wait(0.5 / this.world.worldSpeed).thenRun(() -> {
			// move to the next plot
			this.world.switchPlot(this.worldState.currentPlot + 1);
			this.world.state = SimulatingTurn;
		});
	}

	function trigger(plot: Plot, crop: Crop) {
		switch (crop.state.cropStage) {
			case Seed:
				crop.state.timer -= 1;
			case Growing:
				final context = new CropSystemContext(this.world, crop, plot);
				crop.crop.onGrow(context);
			case Matured:
				crop.state.timer -= 1;
			case Withered:
		}
		if (crop.state.timer == 0) {
			final context = new CropSystemContext(this.world, crop, plot);
			switch (crop.state.cropStage) {
				case Seed:
					crop.state.cropStage = Growing;
					crop.state.timer = crop.crop.getGrowthTurn(context);
				case Growing:
					crop.state.cropStage = Matured;
					crop.state.timer = crop.crop.getWitherTurn(context);
					crop.crop.onMatured(context);
				case Matured:
					crop.state.cropStage = Withered;
					crop.crop.onWithered(context);
				default:
			}
		}
		plot.render.sync();
	}
}
