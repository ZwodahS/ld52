package world.systems;

class CardSystem extends System {
	var updater: zf.up.Updater;

	public function new() {
		super();
		this.updater = new zf.up.Updater();
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);
	}

	override public function onEvent(event: hxd.Event): Bool {
		return false;
	}

	/**
		reset the system to the same state after constructor
	**/
	override public function reset() {
		super.reset();
	}

	/**
		Dispose the system data
	**/
	override public function dispose() {
		super.dispose();
	}

	/**
		update loop
	**/
	override public function update(dt: Float) {
		super.update(dt);
		this.updater.update(dt);
		if (this.world.state != PlayingCard) return;
		if (this.updater.count != 0) return;
		this.world.state = EndOfPlayingCard;
	}

	public function playCard(card: Card) {
		final context = new CardContext(this.world, card, this);
		this.world.state = PlayingCard;
		card.card.onPlay(context);
	}

	public function plantCrop(id: String) {
		final plot = this.worldState.plots[this.worldState.currentPlot];
		if (plot.plot.crop != null) return;
		final crop: Crop = cast this.world.rules.entities[id].make(this.world.nextId, this.worldState);
		plot.plot.crop = crop;
		final context = new CropContext(this.world, crop);
		crop.state.value = crop.crop.getBaseValue(context);
		crop.state.cropStage = Seed;
		crop.state.timer = 1;
	}
}
