package world.systems;

class RenderSystem extends System {
	// ---- All the various layers to draw into ---- //

	/**
		The main draw layer
	**/
	public final drawLayers: h2d.Layers;

	/**
		Background layers.
	**/
	public final bgLayers: h2d.Layers;

	/**
		Window layers, used by windowRenderSystem
	**/
	public final windowLayers: h2d.Layers;

	/**
		World rendering
	**/
	public final worldLayers: h2d.Layers;

	/**
		Hud
	**/
	public final hudLayers: h2d.Layers;

	/**
		Debug layers.
	**/
	public final worldDebugLayers: h2d.Layers;

	public static final LDebug = 100;

	/**
		A non blocking animator.

		Use this for animation that is non-blocking
	**/
	public final animator: zf.up.Updater;

	/**
		WindowRenderSystem, a sub system for rendering windows
	**/
	public final windowRenderSystem: zf.ui.WindowRenderSystem;

	/**
		Tooltip Helper to attach tooltip to any h2d.Object.

		For game entity, it is better to use tooltip component.
	**/
	public final tooltipHelper: zf.ui.TooltipHelper;

	public var bg: h2d.Bitmap;

	public final startDay: zf.ui.Button;

	public var goldValue: h2d.HtmlText;
	public var dayValue: h2d.HtmlText;

	public function new() {
		super();
		this.animator = new zf.up.Updater();
		this.shuffleAnimator = new zf.up.Updater();

		// ---- Setup all the various layers ---- //
		this.drawLayers = new h2d.Layers();
		this.drawLayers.add(this.bgLayers = new h2d.Layers(), 0);
		this.drawLayers.add(this.worldLayers = new h2d.Layers(), 50);
		this.drawLayers.add(this.hudLayers = new h2d.Layers(), 60);
		this.drawLayers.add(this.windowLayers = new h2d.Layers(), 100);

		this.worldLayers.add(this.worldDebugLayers = new h2d.Layers(), LDebug);

		// background
		this.bg = Assets.fromColor(0xff3f7082, Globals.game.gameWidth, Globals.game.gameHeight);
		this.bgLayers.addChild(bg);

		this.startDay = new ui.BasicButton("ui.game.buttons.startDay", [60, 15]);
		this.startDay.addOnClickListener("RenderSystem", (e) -> {
			this.world.startDay();
		});
		this.startDay.setX(Globals.game.gameWidth, AnchorRight, 10).setY(Globals.game.gameHeight, AnchorBottom, 50);
		this.hudLayers.addChild(this.startDay);
		// ---- Setup WindowRenderSystem ---- //
		final windowBounds = new h2d.col.Bounds();
		windowBounds.xMin = 15;
		windowBounds.yMin = 15;
		windowBounds.xMax = Globals.game.gameWidth - 15;
		windowBounds.yMax = Globals.game.gameHeight - 15;
		this.windowRenderSystem = new zf.ui.WindowRenderSystem(windowBounds, this.windowLayers);
		this.windowRenderSystem.defaultRenderDirection = [Down, Right, Left, Up];
		this.windowRenderSystem.defaultSpacing = 5;

		this.tooltipHelper = new zf.ui.TooltipHelper(this.windowRenderSystem);
	}

	override public function init(world: zf.engine2.World) {
		super.init(world);

		// @:listen RenderSystem MOnWorldStateSet 50
		dispatcher.listen(MOnWorldStateSet.MessageType, (message: zf.Message) -> {
			onLoad();
		}, 50);
	}

	override public function reset() {
		this.animator.clear();
		this.windowLayers.removeChildren();
		this.worldLayers.removeChildren();
	}

	override public function update(dt: Float) {
		super.update(dt);
		if (this.worldState != null) {
			var rentText = ' (Rent: First day free)';
			if (this.worldState.rent != 0) {
				rentText = ' (Rent: -${this.worldState.rent})'.font(Colors.Reds[0]);
			}
			this.goldValue.text = '${this.worldState.gold}' + rentText;
		}
		this.dayValue.text = 'Day ${this.worldState.day}     ';
		this.animator.update(dt);
		this.shuffleAnimator.update(dt);
	}

	// ---- Load the world after it is set ---- //
	function onLoad() {
		reset();
		// get all the plots and render them onto the screen
		renderHud();
		renderPlots();
		renderCards();
	}

	// ---- Hud Rendering ---- //
	function renderHud() {
		final flow = new h2d.Flow();
		this.dayValue = new h2d.HtmlText(Assets.displayFonts[0]);
		flow.addChild(this.dayValue);

		flow.layout = Horizontal;
		flow.horizontalSpacing = 4;
		flow.verticalAlign = Middle;
		flow.addChild(Assets.res.getBitmap("plot:icon:gold"));
		flow.addChild(this.goldValue = new h2d.HtmlText(Assets.displayFonts[0]));
		flow.x = 5;
		flow.y = 5;

		this.hudLayers.addChild(flow);
	}

	// ---- Plots rendering ---- //
	function renderPlots() {
		if (this.worldState == null) return;
		final display = new h2d.Object();

		final positions: Array<Point2i> = [[0, 0], [1, 0], [2, 0], [2, 1], [2, 2], [1, 2], [0, 2], [0, 1]];

		for (i => plot in this.worldState.plots) {
			final rc: PlotRenderComponent = cast plot.render;
			rc.plotDisplay.x = positions[i].x * (Constants.GridSize.x + 5);
			rc.plotDisplay.y = positions[i].y * (Constants.GridSize.y + 5);
			display.addChild(rc.plotDisplay);
			if (i == this.worldState.currentPlot) {
				rc.highlightColor = 0xffffeca3;
			} else {
				rc.highlightColor = null;
			}
		}

		this.worldLayers.addChild(display);
		display.setX(Globals.game.gameWidth, AlignCenter).setY(15);
	}

	var deckDisplay: h2d.Object;

	function renderCards() {
		if (this.worldState == null) return;
		final display = new h2d.Object();

		for (ind => card in this.worldState.deck) {
			final rc: CardRenderComponent = cast card.render;
			display.addChild(rc.deckDisplay);
			rc.deckDisplay.x = ind * 25;
			bindCard(card, rc);
		}

		this.worldLayers.addChild(display);
		final bounds = display.getBounds();
		display.setX(Globals.game.gameWidth, AlignCenter, -(bounds.xMin));
		display.setY(Globals.game.gameHeight, AnchorBottom, 10 + (bounds.yMin));
		this.deckDisplay = display;
	}

	var hoverCard: h2d.Object;

	function bindCard(card: Card, rc: CardRenderComponent) {
		rc.deckDisplay.addOnClickListener("RenderSystem", (e) -> {
			onDeckCardClicked(card);
		});
		rc.deckDisplay.addOnOverListener("RenderSystem", (e) -> {
			if (hoverCard != null) hoverCard.remove();
			final bounds = rc.deckDisplay.getBounds();
			this.hoverCard = rc.cardinfo;
			this.windowRenderSystem.showWindow(rc.cardinfo, bounds, {
				preferredDirection: [Up, Right, Left, Down],
			});
		});
		rc.deckDisplay.addOnOutListener("RenderSystem", (e) -> {
			hideCard();
		});
	}

	public function hideCard() {
		if (this.hoverCard == null) return;
		this.hoverCard.remove();
		this.hoverCard = null;
	}

	public var shuffleAnimator: zf.up.Updater;

	// ---- Logic ---- //
	public function shuffleDeck(onFinish: Void->Void) {
		if (this.worldState == null) {
			onFinish();
			return;
		}
		final bounds = this.deckDisplay.getBounds();
		final centerX = (bounds.width / 2) - bounds.xMin;
		final moves: Array<Updatable> = [];
		for (card in this.worldState.deck) {
			final rc: CardRenderComponent = cast card.render;
			moves.push(new MoveToLocationByDuration(rc.deckDisplay.wo(), [centerX, rc.deckDisplay.y], .4));
		}
		final batch = new zf.up.Batch(moves).wait(1 / this.world.worldSpeed).whenDone(onShuffleFinish.bind(onFinish));
		this.shuffleAnimator.run(batch);
	}

	function onShuffleFinish(onFinish: Void->Void) {
		// shuffle the deck
		this.worldState.deck.shuffle(this.worldState.r);
		// move them to their correct position
		final moves: Array<Updatable> = [];
		for (ind => card in this.worldState.deck) {
			final rc: CardRenderComponent = cast card.render;
			moves.push(new MoveToLocationByDuration(rc.deckDisplay.wo(), [ind * 25, rc.deckDisplay.y], .4));
		}
		final batch: Batch = new zf.up.Batch(moves);
		batch.whenDone(onFinish);
		this.shuffleAnimator.run(batch);
	}

	function onDeckCardClicked(card: Card) {
		if (this.world.state == Drafting) {
			this.world.draftSystem.onDeckCardChosen(card);
		}
	}

	public function swapCardWithCard(ind: Int, deckCard: Card, newCard: Card) {
		final rc: CardRenderComponent = cast deckCard.render;
		rc.deckDisplay.remove();

		final rc: CardRenderComponent = cast newCard.render;
		bindCard(newCard, rc);
		rc.deckDisplay.x = ind * 25;
		this.deckDisplay.addChild(rc.deckDisplay);
	}
}
