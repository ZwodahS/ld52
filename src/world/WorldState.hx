package world;

typedef WorldStateSF = {}

class WorldState implements StructSerialisable implements Identifiable {
	public var r: hxd.Rand;

	// ---- Id generation ---- //

	/**
		Int counter for id generation
	**/
	var intCounter: zf.IntCounter.SimpleIntCounter;

	public var nextId(get, never): Int;

	public function get_nextId(): Int {
		return this.intCounter.getNextInt();
	}

	/**
		identifier
	**/
	public function identifier() {
		return "WorldState";
	}

	var rules: Rules;

	public var plots: Array<Plot>;

	public var deck: Array<Card>;

	public var currentPlot: Int = 0;

	public var currentPlaying: Int = -1;

	public var gold: Int = 0;

	public var day: Int = 0;

	public var rent(get, never): Int;

	function get_rent(): Int {
		return Math.clampI((this.day) * 3, 0, 60);
	}

	public function new(rules: Rules, seed: Int = 0) {
		this.rules = rules;
		this.intCounter = new zf.IntCounter.SimpleIntCounter();
		this.r = new hxd.Rand(seed);
		this.plots = [];
		this.deck = [];
	}

	// ---- Save / Load ---- //
	public function toStruct(context: SerialiseContext, option: SerialiseOption): WorldStateSF {
		return this.rules.toStruct(context, option, this);
	}

	public function loadStruct(context: SerialiseContext, option: SerialiseOption, data: Dynamic): WorldState {
		this.rules.loadStruct(context, option, this, data);
		return this;
	}

	public function getPlot(ind: Int): Plot {
		if (ind < 0) ind += this.plots.length;
		final plot = this.plots[ind % this.plots.length];
		return plot;
	}
}
