package world;

class World extends zf.engine2.World {
	/**
		Store the main rule object
	**/
	public var rules: Rules;

	// ---- Systems ---- //

	/**
		Main rendering system
	**/
	public var renderSystem: RenderSystem;

	public var cardSystem: CardSystem;
	public var cropSystem: CropSystem;
	public var draftSystem: DraftSystem;
	public var turnSystem: TurnSystem;

	/**
		World State
	**/
	public var worldState(default, set): WorldState = null;

	public function set_worldState(s: WorldState): WorldState {
		this.worldState = s;
		this.dispatcher.dispatch(new MOnWorldStateSet());
		this.state = Idle;
		return this.worldState;
	}

	public var nextId(get, never): Int;

	inline function get_nextId(): Int {
		return this.worldState == null ? 0 : this.worldState.nextId;
	}

	/**
		User Profile
	**/
	public var profile: Profile;

	/**
		World simulation speed.
	**/
	public var worldSpeed: Float = 2;

	public var state: GameState = Init;
	public var pause: Bool = false;

	public function new(rules: Rules, profile: Profile) {
		super();
		this.rules = rules;
		this.profile = profile;
		this.addSystem(this.renderSystem = new RenderSystem());
		this.addSystem(this.cardSystem = new CardSystem());
		this.addSystem(this.cropSystem = new CropSystem());
		this.addSystem(this.draftSystem = new DraftSystem());
		this.addSystem(this.turnSystem = new TurnSystem());
	}

	// ---- Core Logic ---- //
	public function startDay() {
		if (this.worldState == null) return;
		this.turnSystem.startDay();
	}

	public function payRent() {
		this.worldState.gold -= this.worldState.rent;
		this.worldState.day += 1;
		startDraft();
	}

	public function startDraft() {
		this.draftSystem.showDraft();
	}

	public function restartGame() {
		final worldState = Globals.rules.newGame();
		final world = new World(this.rules, this.profile);
		world.worldState = worldState;
		Globals.game.switchScreen(new screens.GameScreen(world));
	}

	public function switchPlot(i: Int) {
		final plot: Plot = this.worldState.plots[this.worldState.currentPlot];
		final rc: PlotRenderComponent = cast plot.render;
		rc.highlightColor = null;

		this.worldState.currentPlot = (i + this.worldState.plots.length) % this.worldState.plots.length;

		final plot: Plot = this.worldState.plots[this.worldState.currentPlot];
		final rc: PlotRenderComponent = cast plot.render;
		rc.highlightColor = 0xffffeca3;
	}
}
