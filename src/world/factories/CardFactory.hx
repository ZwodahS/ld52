package world.factories;

class CardFactory extends EntityFactory {
	public var conf: CardConf;

	public function new(rules: Rules, conf: CardConf) {
		super(conf.id, rules);
		this.conf = conf;
	}

	override public function make(id: Int, worldState: WorldState, conf: Dynamic = null): Entity {
		final card = new Card(id, this.typeId, this.conf);
		card.rules = this.rules;
		return card;
	}
}
