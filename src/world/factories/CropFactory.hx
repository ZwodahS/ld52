package world.factories;

class CropFactory extends EntityFactory {
	public var conf: CropConf;

	public function new(rules: Rules, conf: CropConf) {
		super(conf.id, rules);
		this.conf = conf;
	}

	override public function make(id: Int, worldState: WorldState, conf: Dynamic = null): Entity {
		final crop = new Crop(id, this.typeId, this.conf);
		crop.rules = this.rules;
		return crop;
	}
}
