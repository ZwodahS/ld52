package world.rules.confs;

typedef CardConf = {
	public var id: String;
	public var stringId: String;

	public var render: {
		public var ?crop: {
			public var value: Int;
			public var water: Int;
			public var growth: Int;
			public var wither: Int;
		};
		public var bitmapId: String;
	};
	public var ?onPlay: (CardContext) -> Void;
}
