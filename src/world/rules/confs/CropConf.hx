package world.rules.confs;

typedef CropConf = {
	public var id: String;
	public var stringId: String;
	public var ?valueMultiplier: Int;
	public var ?persist: Bool;

	public var render: {
		public var bitmapId: String;
	};
	public var ?getBaseValue: (CropContext) -> Int;
	public var ?getRequiredWater: (CropContext) -> Int;
	public var ?getGrowthTurn: (CropContext) -> Int;
	public var ?getWitherTurn: (CropContext) -> Int;
	public var ?onGrow: (CropSystemContext) -> Void;
	public var ?onMatured: (CropSystemContext) -> Void;
	public var ?onWithered: (CropSystemContext) -> Void;
	public var ?canHarvest: (CropSystemContext) -> Bool;
}
