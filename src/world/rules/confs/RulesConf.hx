package world.rules.confs;

typedef RulesConf = {
	public var crops: Array<String>;
	public var cards: Array<String>;
}
