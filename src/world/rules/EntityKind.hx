package world.rules;

enum abstract EntityKind(String) from String to String {
	// Unknown, the entity is not inited.
	public var Unknown = "EK_Unknown";
	public var Plot = "EK_Plot";
	public var Crop = "EK_Crop";
	public var Card = "EK_Card";
}
