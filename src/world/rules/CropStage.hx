package world.rules;

enum abstract CropStage(String) from String to String {
	public var Seed = "CS_Seed";
	public var Growing = "CS_Growing";
	public var Matured = "CS_Matured";
	public var Withered = "CS_Withered";
}
