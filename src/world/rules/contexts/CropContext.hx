package world.rules.contexts;

class CropContext extends Context {
	public var crop: Crop;

	public function new(world: World, crop: Crop) {
		super(world);
		this.crop = crop;
	}
}
