package world.rules.contexts;

class CardContext extends Context {
	var card: Card;
	var system: CardSystem;

	public function new(world: World, card: Card, system: CardSystem) {
		super(world);
		this.card = card;
		this.system = system;
	}

	public function plantCrop(cropId: String) {
		this.system.plantCrop(cropId);
	}
}
