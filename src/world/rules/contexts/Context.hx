package world.rules.contexts;

class Context {
	var world: World;

	public var hour: Int;

	public function new(world: World) {
		this.world = world;
		this.hour = this.world.worldState.currentPlaying;
	}

	public function increaseWaterAll(amount: Int) {
		for (plot in this.world.worldState.plots) {
			plot.plot.water += amount;
		}
	}

	public function increaseWaterAdjacent(amount: Int) {
		final plot = this.world.worldState.getPlot(this.world.worldState.currentPlot + 1);
		plot.plot.water += amount;
		final plot = this.world.worldState.getPlot(this.world.worldState.currentPlot - 1);
		plot.plot.water += amount;
	}

	public function increaseWaterCurrent(amount: Int) {
		final plot = this.world.worldState.getPlot(this.world.worldState.currentPlot);
		plot.plot.water += amount;
	}

	public function increaseNutrientAll(amount: Int) {
		for (plot in this.world.worldState.plots) {
			plot.plot.shit += amount;
		}
	}

	public function increaseNutrientAdjacent(amount: Int) {
		final plot = this.world.worldState.getPlot(this.world.worldState.currentPlot + 1);
		plot.plot.shit += amount;
		final plot = this.world.worldState.getPlot(this.world.worldState.currentPlot - 1);
		plot.plot.shit += amount;
	}

	public function increaseNutrientCurrent(amount: Int) {
		final plot = this.world.worldState.getPlot(this.world.worldState.currentPlot);
		plot.plot.shit += amount;
	}

	public function moveForward(amount: Int) {
		this.world.switchPlot(this.world.worldState.currentPlot + amount);
	}

	public function moveBackward(amount: Int) {
		this.world.switchPlot(this.world.worldState.currentPlot - amount);
	}

	public function triggerHarvest() {
		for (plot in this.world.worldState.plots) {
			if (plot.plot.crop == null) continue;
			this.world.turnSystem.harvestCrop(plot, plot.plot.crop);
		}
	}

	public function getAdjacentPlots(): Array<Plot> {
		return [
			this.world.worldState.getPlot(this.world.worldState.currentPlot - 1),
			this.world.worldState.getPlot(this.world.worldState.currentPlot + 1),
		];
	}
}
