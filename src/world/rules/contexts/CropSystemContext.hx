package world.rules.contexts;

class CropSystemContext extends CropContext {
	public var plot: Plot;

	public function new(world: World, crop: Crop, plot: Plot) {
		super(world, crop);
		this.plot = plot;
	}

	public function depleteWater(amount: Int): Bool {
		if (amount > plot.plot.water) return false;
		plot.plot.water -= amount;
		return true;
	}

	public function depleteNutrient(amount: Int): Bool {
		if (amount > plot.plot.shit) return false;
		plot.plot.shit -= amount;
		return true;
	}

	public function depleteWaterAndNutrient(w: Int, n: Int): Bool {
		if (w > plot.plot.water || n > plot.plot.shit) return false;
		plot.plot.water -= w;
		plot.plot.shit -= n;
		return true;
	}

	public function increaseWaterAdjacentToPlot(amount: Int, plot: Plot) {
		final plotIndex = this.world.worldState.plots.indexOf(plot);
		final plot = this.world.worldState.getPlot(plotIndex + 1);
		plot.plot.water += amount;
		final plot = this.world.worldState.getPlot(plotIndex - 1);
		plot.plot.water += amount;
	}

	public function increaseNutrientAdjacentToPlot(amount: Int, plot: Plot) {
		final plotIndex = this.world.worldState.plots.indexOf(plot);
		final plot = this.world.worldState.getPlot(plotIndex + 1);
		plot.plot.shit += amount;
		final plot = this.world.worldState.getPlot(plotIndex - 1);
		plot.plot.shit += amount;
	}

	override public function getAdjacentPlots(): Array<Plot> {
		final plotIndex = this.world.worldState.plots.indexOf(this.plot);
		return [
			this.world.worldState.getPlot(plotIndex - 1),
			this.world.worldState.getPlot(plotIndex + 1),
		];
	}
}
