package world;

import world.factories.EntityFactory;

class Rules {
	public var entities: Map<String, EntityFactory>;
	public var cards: Array<CardFactory>;

	public var structLoader: zf.StructLoader;

	public var interp: hscript.Interp;
	public var parser: hscript.Parser;

	public function new() {
		this.entities = new Map<String, EntityFactory>();
		this.cards = [];

		// ---- Set up struct loader ---- //
		this.structLoader = new zf.StructLoader();

		// ---- Set up hscript ---- //
		this.parser = new hscript.Parser();
		this.interp = new hscript.Interp();
	}

	// ---- Make / Save / Load ---- //
	public function newGame(): WorldState {
		final state = new WorldState(this, Random.int(0, zf.Constants.SeedMax));

		for (_ in 0...8) {
			final plot = new Plot(state.nextId);
			plot.plot.water = 100;
			state.plots.push(plot);
		}

		state.deck.push(cast entities["card:carrot"].make(state.nextId, state));
		state.deck.push(cast entities["card:carrot"].make(state.nextId, state));
		state.deck.push(cast entities["card:carrot"].make(state.nextId, state));
		state.deck.push(cast entities["card:carrot"].make(state.nextId, state));
		state.deck.push(cast entities["card:potato"].make(state.nextId, state));
		state.deck.push(cast entities["card:potato"].make(state.nextId, state));
		state.deck.push(cast entities["card:potato"].make(state.nextId, state));
		state.deck.push(cast entities["card:potato"].make(state.nextId, state));
		state.deck.push(cast entities["card:waterall"].make(state.nextId, state));
		state.deck.push(cast entities["card:waterall"].make(state.nextId, state));
		state.deck.push(cast entities["card:wateradjacent"].make(state.nextId, state));
		state.deck.push(cast entities["card:fertiliseadjacent"].make(state.nextId, state));
		return state;
	}

	public function loadConfig(path: String) {
		final configPath = new haxe.io.Path(path);
		final expr = this.structLoader.loadFile(path);
		final ast = this.parser.parseString(expr);
		final defaultConf: RulesConf = this.interp.execute(ast);

		loadCards(configPath, defaultConf);
		loadCrops(configPath, defaultConf);
	}

	function loadCards(configPath: haxe.io.Path, conf: RulesConf) {
		var count = 0;
		for (p in conf.cards) {
			final path = haxe.io.Path.join([configPath.dir, p]);
			final confs: Array<Dynamic> = cast exec(path);
			for (conf in confs) {
				try {
					final factory = new CardFactory(this, conf);
					this.cards.push(factory);
					this.entities[factory.typeId] = factory;
					count += 1;
				} catch (e) {
					Logger.exception(e);
					Logger.warn('Fail to load card: ${path}');
				}
			}
		}

		Logger.info('${count} cards loaded.');
	}

	function loadCrops(configPath: haxe.io.Path, conf: RulesConf) {
		var count = 0;
		for (p in conf.crops) {
			final path = haxe.io.Path.join([configPath.dir, p]);
			final confs: Array<Dynamic> = cast exec(path);
			for (conf in confs) {
				try {
					final factory = new CropFactory(this, conf);
					this.entities[factory.typeId] = factory;
					count += 1;
				} catch (e) {
					Logger.exception(e);
					Logger.warn('Fail to load crop: ${path}');
				}
			}
		}

		Logger.info('${count} crops loaded.');
	}

	function exec(path: String): Dynamic {
		try {
			final expr = this.structLoader.loadFile(path);
			final ast = this.parser.parseString(expr);
			return this.interp.execute(ast);
		} catch (e) {
			Logger.exception(e);
			Logger.warn('Fail to parse: ${path}');
			return null;
		}
	}

	public function loadFromPath(userdata: UserData, path: String): WorldState {
		// this is hard to do since we cannot read directory on web
		// we will need to construct the folder ourselves rather than recursively load it in WorldSaveFolder
		final fullPath = haxe.io.Path.join([path, "world.json"]);
		final result = userdata.loadFromPath(fullPath);
		var data: Dynamic = null;
		switch (result) {
			case SuccessContent(stringData):
				data = haxe.Json.parse(stringData);
			default:
				Logger.warn('Fail to load');
				return null;
		}
		return this.load(data);
	}

	public function load(data: Dynamic): WorldState {
		final context = new SerialiseContext();
		final option: SerialiseOption = {};
		final state = new WorldState(this, Random.int(0, zf.Constants.SeedMax));
		state.loadStruct(context, option, data);
		return state;
	}

	public function loadStruct(context: SerialiseContext, option: SerialiseOption, state: WorldState,
			data: Dynamic): WorldState {
		return state;
	}

	public function saveToPath(userdata: UserData, worldState: WorldState, path: String) {
		final context = new SerialiseContext();
		final worldStateSF = worldState.toStruct(context, {});
		final fullpath = haxe.io.Path.join([path, "world.json"]);
#if sys
		final jsonString = haxe.format.JsonPrinter.print(worldStateSF, "  ");
#else
		final jsonString = haxe.Json.stringify(worldStateSF);
#end
		final result = userdata.saveToPath(fullpath, jsonString);
		switch (result) {
			case Success:
			default:
				Logger.warn('Fail to save');
		}
	}

	public function toStruct(context: SerialiseContext, option: SerialiseOption,
			worldState: WorldState): WorldStateSF {
		return {};
	}
}
