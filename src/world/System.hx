package world;

class System extends zf.engine2.System {
	// ---- Aliases ---- //
	public var world(get, never): World;

	inline function get_world()
		return cast this.__world__;

	public var worldState(get, never): WorldState;

	inline function get_worldState()
		return this.world == null ? null : this.world.worldState;
}
