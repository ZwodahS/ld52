package world.components;

class CropComponent extends Component {
	public static final ComponentType = "CropComponent";

	public var conf: CropConf;

	public var valueMultiplier: Int = 1;
	public var persist: Bool = false;

	public function new(cropConf: CropConf) {
		this.conf = cropConf;

		if (this.conf.valueMultiplier != null) this.valueMultiplier = conf.valueMultiplier;
		if (this.conf.persist != null) this.persist = conf.persist;

		if (this.conf.getBaseValue != null) this.getBaseValue = conf.getBaseValue;
		if (this.conf.getRequiredWater != null) this.getRequiredWater = conf.getRequiredWater;
		if (this.conf.getGrowthTurn != null) this.getGrowthTurn = conf.getGrowthTurn;
		if (this.conf.getWitherTurn != null) this.getWitherTurn = conf.getWitherTurn;

		if (this.conf.onGrow != null) this.onGrow = conf.onGrow;
		if (this.conf.onMatured != null) this.onMatured = conf.onMatured;
		if (this.conf.onWithered != null) this.onWithered = conf.onWithered;
		if (this.conf.canHarvest != null) this.canHarvest = conf.canHarvest;
	}

	dynamic public function getBaseValue(ctx: CropContext): Int {
		return 0;
	}

	dynamic public function getRequiredWater(ctx: CropContext): Int {
		return 0;
	}

	dynamic public function getGrowthTurn(ctx: CropContext): Int {
		return 3;
	}

	dynamic public function getWitherTurn(ctx: CropContext): Int {
		return 10;
	}

	dynamic public function onGrow(ctx: CropSystemContext) {
		if (ctx.depleteWater(ctx.crop.crop.getRequiredWater(ctx)) == false) return;
		ctx.crop.state.timer -= 1;
	}

	dynamic public function onMatured(ctx: CropSystemContext) {}

	dynamic public function onWithered(ctx: CropSystemContext) {}

	dynamic public function canHarvest(ctx: CropSystemContext): Bool {
		return true;
	}
}
