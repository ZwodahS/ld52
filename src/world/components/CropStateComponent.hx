package world.components;

class CropStateComponent extends Component {
	public static final ComponentType = "CropStateComponent";

	public var value: Int;
	public var cropStage: CropStage;
	public var timer: Int;

	public function new() {
		this.value = 0;
		this.cropStage = Seed;
	}
}
