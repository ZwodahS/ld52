package world.components;

class CardComponent extends Component {
	public static final ComponentType = "CardComponent";

	public var conf: CardConf;

	public function new(conf: CardConf) {
		this.conf = conf;
		if (this.conf.onPlay != null) this.onPlay = this.conf.onPlay;
	}

	dynamic public function onPlay(context: CardContext) {}
}
