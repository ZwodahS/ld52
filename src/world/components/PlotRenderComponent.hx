package world.components;

class PlotRenderComponent extends RenderComponent {
	public var highlight: h2d.Bitmap;

	public var highlightColor(never, set): Null<Color>;

	public function set_highlightColor(c: Null<Color>): Null<Color> {
		if (c == null) {
			highlight.visible = false;
		} else {
			highlight.visible = true;
			highlight.color.setColor(c);
		}
		return c;
	}

	public var plotDisplay: h2d.Object;

	var cropStage(default, set): Null<CropStage>;

	function set_cropStage(c: Null<CropStage>): Null<CropStage> {
		if (this.cropStage == c) return this.cropStage;
		this.cropStage = c;
		if (this.cropStageIcon != null) this.cropStageIcon.remove();
		if (this.cropStage == null) return null;
		final icon = switch (this.cropStage) {
			case Seed: Assets.res.getBitmap("cropstage:seed");
			case Growing: Assets.res.getBitmap("cropstage:growing");
			case Matured: Assets.res.getBitmap("cropstage:matured");
			case Withered: Assets.res.getBitmap("cropstage:withered");
		}
		this.cropStageIcon = icon;
		this.plotDisplay.addChild(this.cropStageIcon);
		cropStageIcon.setX(23).setY(23);
		return this.cropStage;
	}

	var cropStageIcon: h2d.Object;

	var shitIcon: h2d.Object;
	var waterIcon: h2d.Object;
	var timerIcon: h2d.Object;
	var goldIcon: h2d.Object;

	var shitValueDisplay: h2d.Object;
	var waterValueDisplay: h2d.Object;
	var timerValueDisplay: h2d.Object;
	var goldValueDisplay: h2d.Object;

	var shitValue(default, set): Int;

	public var cropIcon: h2d.Bitmap;

	function set_shitValue(v: Int): Int {
		if (this.shitValue == v) return v;
		this.shitValue = v;
		this.shitValueDisplay.removeChildren();
		final str = '${this.shitValue}';
		displayStr(str, this.shitValueDisplay);
		return this.shitValue;
	}

	var waterValue(default, set): Int;

	function set_waterValue(v: Int): Int {
		if (this.waterValue == v) return v;
		this.waterValue = v;
		this.waterValueDisplay.removeChildren();
		final str = '${this.waterValue}';
		displayStr(str, this.waterValueDisplay);
		return this.waterValue;
	}

	var timerValue(default, set): Int;

	function set_timerValue(v: Int): Int {
		if (this.timerValue == v) return v;
		this.timerValue = v;
		this.timerValueDisplay.removeChildren();
		final str = '${this.timerValue}';
		displayStr(str, this.timerValueDisplay);
		return this.timerValue;
	}

	var goldValue(default, set): Int;

	function set_goldValue(v: Int): Int {
		if (this.goldValue == v) return v;
		this.goldValue = v;
		this.goldValueDisplay.removeChildren();
		final str = '${this.goldValue}';
		displayStr(str, this.goldValueDisplay);
		return this.goldValue;
	}

	public function new() {
		super();
	}

	override function onEntityUpdated() {
		this.plotDisplay = new h2d.Object();
		this.plotDisplay.addChild(Assets.res.getBitmap("plot:base"));

		this.plotDisplay.addChild(this.waterIcon = Assets.res.getBitmap("plot:icon:water"));
		this.waterIcon.setX(4).setY(21);
		this.plotDisplay.addChild(this.shitIcon = Assets.res.getBitmap("plot:icon:shit"));
		this.shitIcon.setX(4).setY(28);

		this.plotDisplay.addChild(this.timerIcon = Assets.res.getBitmap("plot:icon:time"));
		this.timerIcon.setX(19).setY(4);
		this.plotDisplay.addChild(this.goldIcon = Assets.res.getBitmap("plot:icon:gold"));
		this.goldIcon.setX(19).setY(11);

		this.plotDisplay.addChild(this.shitValueDisplay = new h2d.Object());
		this.plotDisplay.addChild(this.waterValueDisplay = new h2d.Object());
		this.plotDisplay.addChild(this.timerValueDisplay = new h2d.Object());
		this.plotDisplay.addChild(this.goldValueDisplay = new h2d.Object());
		this.shitValueDisplay.setX(11).setY(28);
		this.waterValueDisplay.setX(11).setY(21);
		this.timerValueDisplay.setX(26).setY(4);
		this.goldValueDisplay.setX(26).setY(11);
		this.shitValue = -1;
		this.waterValue = -1;
		this.timerValue = -1;
		this.goldValue = -1;

		this.highlight = Assets.res.getBitmap("plot:highlight");
		this.plotDisplay.addChild(this.highlight);
		sync();
	}

	override public function sync() {
		final plot: Plot = cast this.entity;
		this.waterValue = plot.plot.water;
		this.shitValue = plot.plot.shit;
		if (plot.plot.crop == null) {
			this.timerValue = 0;
			this.goldValue = 0;
			this.timerValueDisplay.visible = false;
			this.goldValueDisplay.visible = false;
			this.timerIcon.visible = false;
			this.goldIcon.visible = false;
			this.cropStage = null;
			if (this.cropIcon != null) this.cropIcon.remove();
			this.cropIcon = null;
		} else {
			this.timerValue = plot.plot.crop.state.timer;
			this.goldValue = plot.plot.crop.state.value;
			this.timerValueDisplay.visible = true;
			this.goldValueDisplay.visible = true;
			this.timerIcon.visible = true;
			this.goldIcon.visible = true;
			this.cropStage = plot.plot.crop.state.cropStage;
			if (this.cropIcon == null) {
				this.cropIcon = Assets.res.getBitmap(plot.plot.crop.crop.conf.render.bitmapId);
				this.plotDisplay.addChild(this.cropIcon);
				this.cropIcon.setX(2).setY(2);
			}
		}
	}

	function displayStr(str: String, display: h2d.Object) {
		for (i => s in str) {
			var bm = Assets.res.getBitmap('n:${String.fromCharCode(s)}');
			if (bm == null) bm = Assets.res.getBitmap('n: ');
			bm.x = i * 4;
			display.addChild(bm);
		}
	}
}
