package world.components;

class PlotComponent extends Component {
	public var crop(default, set): Crop;

	public function set_crop(c: Crop): Crop {
		this.crop = c;
		final plot: Plot = cast this.entity;
		if (plot != null && plot.render != null) plot.render.sync();
		return this.crop;
	}

	public var water(default, set): Int;

	public function set_water(v: Int): Int {
		this.water = Math.clampI(v, 0, 100);
		final plot: Plot = cast this.entity;
		if (plot != null && plot.render != null) plot.render.sync();
		return this.water;
	}

	public var shit(default, set): Int;

	public function set_shit(v: Int): Int {
		this.shit = v;
		final plot: Plot = cast this.entity;
		if (plot != null && plot.render != null) plot.render.sync();
		return this.shit;
	}

	public function new() {
		this.water = 0;
		this.shit = 0;
	}
}
