package world.components;

class CardInfo extends zf.ui.UIElement {
	public var card: Card;

	public function new(card: Card) {
		super();
		this.card = card;

		var cropDetails = card.card.conf.render.crop;
		var bm = Assets.res.getBitmap(cropDetails != null ? "card:big:cropbase" : "card:big:base");
		this.addChild(bm);

		if (cropDetails != null) {
			this.addChild(displayStr('${cropDetails.value}', new h2d.Object()).setX(8).setY(57));
			this.addChild(displayStr('${cropDetails.water}', new h2d.Object()).setX(8).setY(65));
			this.addChild(displayStr('${cropDetails.growth}', new h2d.Object()).setX(41).setY(57));
			this.addChild(displayStr('${cropDetails.wither}', new h2d.Object()).setX(41).setY(65));
		}

		final name = Strings.get('${card.card.conf.stringId}.name');
		final nameText = new h2d.HtmlText(Assets.displayFonts[1]);
		nameText.textColor = Colors.Blacks[0];
		nameText.text = name;
		this.addChild(nameText);
		nameText.setX(2).setY(2);

		var icon = Assets.res.getBitmap(card.card.conf.render.bitmapId);
		if (icon != null) {
			icon.putBelow(nameText);
			this.addChild(icon);
		}

		final description = Strings.get('${card.card.conf.stringId}.description');
		if (description != null) {
			final descriptionText = new h2d.HtmlText(Assets.displayFonts[0]);
			descriptionText.maxWidth = 46;
			descriptionText.text = description;
			this.addChild(descriptionText);
			descriptionText.putBelow(icon, [0, 2]);
			descriptionText.textColor = Colors.Blacks[0];
		}
	}

	function displayStr(str: String, display: h2d.Object) {
		for (i => s in str) {
			var bm = Assets.res.getBitmap('n:${String.fromCharCode(s)}');
			if (bm == null) bm = Assets.res.getBitmap('n: ');
			bm.x = i * 4;
			display.addChild(bm);
		}
		return display;
	}
}

class MiniCardDisplay extends zf.ui.UIElement {
	public var card: Card;

	public var highlight: h2d.Bitmap;

	public var highlightColor(never, set): Null<Color>;

	public function set_highlightColor(c: Null<Color>): Null<Color> {
		if (c == null) {
			highlight.visible = false;
		} else {
			highlight.visible = true;
			highlight.color.setColor(c);
		}
		return c;
	}

	public function new(card: Card) {
		super();
		this.card = card;

		final display = new h2d.Object();
		display.addChild(Assets.res.getBitmap("card:base"));
		final bounds = display.getBounds();

		var bm = Assets.res.getBitmap(card.card.conf.render.bitmapId);
		if (bm != null) {
			bm.setX(2).setY(2);
			display.addChild(bm);
		}

		display.center();
		this.addChild(display);
		this.interactive = new Interactive(bounds.width, bounds.height);
		display.addChild(this.interactive);

		this.highlight = Assets.res.getBitmap("card:highlight");
		display.addChild(this.highlight);
		this.highlightColor = null;
	}
}

class CardRenderComponent extends RenderComponent {
	public var deckDisplay: MiniCardDisplay;
	public var cardinfo: h2d.Object;

	public function new() {
		super();
	}

	override public function onEntityUpdated() {
		final card: Card = cast this.entity;
		this.deckDisplay = new MiniCardDisplay(card);
		this.cardinfo = new CardInfo(card);
	}
}
