package world.entities;

class Crop extends Entity {
	public var crop(default, set): CropComponent;

	public function set_crop(component: CropComponent): CropComponent {
		final prev = this.crop;
		this.crop = component;
		onComponentChanged(prev, this.crop);
		return this.crop;
	}

	public var state(default, set): CropStateComponent;

	public function set_state(component: CropStateComponent): CropStateComponent {
		final prev = this.state;
		this.state = component;
		onComponentChanged(prev, this.state);
		return this.state;
	}

	public function new(id: Int, typeId: String, cropConf: CropConf) {
		super(id, typeId);
		this.kind = EntityKind.Crop;

		this.crop = new CropComponent(cropConf);
		this.render = new CropRenderComponent();
		this.state = new CropStateComponent();
	}
}
