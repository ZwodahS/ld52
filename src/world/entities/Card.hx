package world.entities;

class Card extends Entity {
	public var card(default, set): CardComponent;

	public function set_card(component: CardComponent): CardComponent {
		final prev = this.card;
		this.card = component;
		onComponentChanged(prev, this.card);
		return this.card;
	}

	public function new(id: Int, typeId: String, cardConf: CardConf) {
		super(id, typeId);
		this.kind = EntityKind.Card;

		this.card = new CardComponent(cardConf);
		this.render = new CardRenderComponent();
	}
}
