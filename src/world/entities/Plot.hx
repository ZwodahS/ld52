package world.entities;

class Plot extends Entity {
	public var plot(default, set): PlotComponent;

	public function set_plot(component: PlotComponent): PlotComponent {
		final prev = this.plot;
		this.plot = component;
		onComponentChanged(prev, this.plot);
		return this.plot;
	}

	public function new(id: Int = -1) {
		super(id, "plot");
		this.kind = EntityKind.Plot;

		this.plot = new PlotComponent();
		this.render = new PlotRenderComponent();
	}
}
