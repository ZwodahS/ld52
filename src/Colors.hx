/**
	All the color values
**/
class Colors {
	public static final Blacks: Array<Color> = [0xff111012, 0, 0];
	public static final Whites: Array<Color> = [0, 0, 0xfffffbe5];

	public static final Greens: Array<Color> = [0, 0xff6cbf60, 0];
	public static final Reds: Array<Color> = [0xffe67a73, 0xffff5e5e, 0];

	public static final HighlightRed: Color = 0xffff5252;
	public static final HighlightGreen: Color = 0xff44f720;
	public static final HighlightBlue: Color = 0xff68f7fa;
	public static final HighlightYellow: Color = 0xfffff703;
	public static final HighlightOrange: Color = 0xffff9a2e;
	public static final HighlightMagneta: Color = 0xfffe70ff;
}
