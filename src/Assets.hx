/**
	Assets is used to store loaded assets
**/
class Assets {
	public static var displayFont: hxd.res.BitmapFont;
	public static var displayFonts: Array<h2d.Font>;

	public static var bodyFont: hxd.res.BitmapFont;
	public static var bodyFonts: Array<h2d.Font>;

	public static var debugFont: hxd.res.BitmapFont;
	public static var debugFonts: Array<h2d.Font>;

	public static var defaultFont: h2d.Font;

	public static var res: ResourceManager;

	public static var whiteBoxFactory: ScaleGridFactory;
	public static var yellowBoxFactory: ScaleGridFactory;
	public static var redBoxFactory: ScaleGridFactory;
	public static var greenBoxFactory: ScaleGridFactory;
	public static var buttonFactory: ScaleGridFactory;

	public static function load() {
		Assets.res = new ResourceManager();
		Assets.res.addSpritesheet(zf.Assets.loadAseSpritesheetConfig('graphics/packed.json'));

		final gluten = hxd.Res.load("fonts/gluten-medium.fnt").to(hxd.res.BitmapFont);
		final novasquare = hxd.Res.load("fonts/novasquare-regular.fnt").to(hxd.res.BitmapFont);

		Assets.debugFont = gluten;
		Assets.debugFonts = [
			gluten.toSdfFont(4, MultiChannel),
			gluten.toSdfFont(6, MultiChannel),
			gluten.toSdfFont(10, MultiChannel),
			gluten.toSdfFont(12, MultiChannel),
			gluten.toSdfFont(14, MultiChannel),
		];

		Assets.displayFont = novasquare;
		Assets.displayFonts = [
			novasquare.toSdfFont(4, MultiChannel),
			novasquare.toSdfFont(6, MultiChannel),
			novasquare.toSdfFont(8, MultiChannel),
			novasquare.toSdfFont(10, MultiChannel),
			novasquare.toSdfFont(12, MultiChannel),
			novasquare.toSdfFont(14, MultiChannel),
		];

		Assets.bodyFont = novasquare;
		Assets.bodyFonts = [
			novasquare.toSdfFont(4, MultiChannel),
			novasquare.toSdfFont(6, MultiChannel),
			novasquare.toSdfFont(8, MultiChannel),
			novasquare.toSdfFont(10, MultiChannel)
		];

		Assets.defaultFont = Assets.bodyFonts[0];

		Assets.whiteBoxFactory = new ScaleGridFactory(Assets.res.getTile("ui:whitebox"), 5, 5);
		Assets.yellowBoxFactory = new ScaleGridFactory(Assets.res.getTile("ui:yellowbox"), 5, 5);
		Assets.redBoxFactory = new ScaleGridFactory(Assets.res.getTile("ui:redbox"), 5, 5);
		Assets.greenBoxFactory = new ScaleGridFactory(Assets.res.getTile("ui:greenbox"), 5, 5);
		Assets.buttonFactory = new ScaleGridFactory(Assets.res.getTile("ui:button:generic"), 6, 6);
	}

	public static function fromColor(color: Color, width: Float, height: Float): h2d.Bitmap {
		final bm = new h2d.Bitmap(Assets.res.getTile("white"));
		bm.width = width;
		bm.height = height;
		bm.color.setColor(color);
		return bm;
	}
}
