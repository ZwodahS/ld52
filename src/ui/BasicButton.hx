package ui;

class BasicButton extends zf.ui.Button.ObjectsButton {
	public function new(textId: String, size: Point2i) {
		super();
		final objects: Array<h2d.Object> = [];
		objects.push(Assets.buttonFactory.make(size).cSetColor(0xff6cbf60));
		objects.push(Assets.buttonFactory.make(size).cSetColor(0xffc4da88));
		objects.push(Assets.buttonFactory.make(size).cSetColor(0xff2f4a2c));
		objects.push(Assets.buttonFactory.make(size).cSetColor(0xff74c2d1));
		final string = Strings.get(textId);
		zf.ui.Button.fromObjects({
			objects: objects,
			font: Assets.displayFonts[1],
			text: string,
		}, this);
		this.textLabel.y -= 1;
	}
}
