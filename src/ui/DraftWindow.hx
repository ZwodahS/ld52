package ui;

import world.entities.Card;
import world.components.CardRenderComponent;

class DraftWindow extends zf.ui.Window {
	var world: World;

	var cards: Array<Card>;

	public function new(world: World, cards: Array<Card>) {
		super();
		this.world = world;
		this.cards = cards;
	}

	override public function onShow() {
		super.onShow();
		final size: Point2i = [240, 100];
		final bg = Assets.whiteBoxFactory.make(size);
		this.addChild(bg);

		final title = new h2d.HtmlText(Assets.bodyFonts[2]);
		title.textColor = Colors.Blacks[0];
		title.text = Strings.get("ui.game.draft.title");
		this.addChild(title);
		title.setX(size.x, AlignCenter).setY(5);

		final instruction = new h2d.HtmlText(Assets.bodyFonts[0]);
		instruction.textColor = Colors.Blacks[0];
		instruction.text = Strings.get("ui.game.draft.instruction");
		instruction.putBelow(title).setX(10);
		this.addChild(instruction);

		final cardRcs: Array<CardRenderComponent> = [];
		final cardDisplay = new h2d.Object();
		for (ind => card in this.cards) {
			final rc: CardRenderComponent = cast card.render;
			cardRcs.push(rc);
			cardDisplay.addChild(rc.deckDisplay);
			rc.deckDisplay.x = ind * 40;
			bind(card, rc);
		}
		final bounds = cardDisplay.getBounds();
		cardDisplay.setY(40).setX(size.x, AlignCenter, -bounds.xMin);
		this.addChild(cardDisplay);

		final skipButton = new ui.BasicButton("ui.game.draft.skip", [60, 15]);
		skipButton.setX(size.x, AlignCenter).setY(size.y, AnchorBottom, 10);
		skipButton.addOnClickListener("DraftWindow", (e) -> {
			this.remove();
			this.world.draftSystem.draftComplete();
		});
		this.addChild(skipButton);
	}

	var hoverCard: h2d.Object;

	function bind(card: Card, rc: CardRenderComponent) {
		rc.deckDisplay.addOnClickListener("DraftWindow", (e) -> {
			this.world.draftSystem.selectDraftCard(card);
		});
		rc.deckDisplay.addOnOverListener("DraftWindow", (e) -> {
			if (hoverCard != null) hoverCard.remove();
			final bounds = rc.deckDisplay.getBounds();
			this.hoverCard = rc.cardinfo;
			this.world.renderSystem.windowRenderSystem.showWindow(rc.cardinfo, bounds, {
				preferredDirection: [Down, Right, Left, Up],
			});
		});
		rc.deckDisplay.addOnOutListener("DraftWindow", (e) -> {
			if (this.hoverCard == null) return;
			this.hoverCard.remove();
			this.hoverCard = null;
		});
	}

	public function close() {
		this.remove();
		for (card in this.cards) {
			final rc: CardRenderComponent = cast card.render;
			rc.deckDisplay.removeAllListeners("DraftWindow");
		}
	}
}
