package ui;

class GameoverWindow extends zf.ui.Window {
	var world: World;

	public function new(world: World) {
		super();
		this.world = world;
	}

	override public function onShow() {
		super.onShow();
		final restartButton = new ui.BasicButton("ui.game.buttons.restart", [60, 15]);

		final size: Point2i = [120, 50];
		final bg = Assets.whiteBoxFactory.make(size);
		this.addChild(bg);
		restartButton.setX(size.x, AlignCenter).setY(size.y, AlignCenter, 10);
		this.addChild(restartButton);

		final text = new h2d.HtmlText(Assets.displayFonts[2]);
		text.textColor = Colors.Blacks[0];
		text.text = Strings.get("ui.game.text.gameover", {day: this.world.worldState.day});
		this.addChild(text);
		text.setX(size.x, AlignCenter).setY(5);

		restartButton.addOnClickListener("GameoverWindow", (e) -> {
			this.world.restartGame();
			this.remove();
		});
	}
}
