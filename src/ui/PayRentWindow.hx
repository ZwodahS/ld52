package ui;

class PayRentWindow extends zf.ui.Window {
	var world: World;

	public function new(world: World) {
		super();
		this.world = world;
	}

	override public function onShow() {
		super.onShow();
		final rentalAmount = this.world.worldState.rent;
		final payRentButton = new ui.BasicButton(rentalAmount > 0 ? "ui.game.buttons.payRent" : "ui.game.buttons.nextDay",
			[60, 15]);

		final size: Point2i = [120, 50];
		final bg = Assets.whiteBoxFactory.make(size);
		this.addChild(bg);
		payRentButton.setX(size.x, AlignCenter).setY(size.y, AlignCenter, 10);
		this.addChild(payRentButton);

		final text = new h2d.HtmlText(Assets.displayFonts[2]);
		text.textColor = Colors.Blacks[0];
		if (rentalAmount == 0) {
			text.text = Strings.get("ui.game.text.noRentToPay");
		} else {
			text.text = Strings.get("ui.game.text.payRent", {amount: rentalAmount});
		}
		this.addChild(text);
		text.setX(size.x, AlignCenter).setY(5);

		payRentButton.addOnClickListener("PayRentWindow", (e) -> {
			this.world.payRent();
			this.remove();
		});
	}
}
