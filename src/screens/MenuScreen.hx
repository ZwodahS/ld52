package screens;

class MenuScreen extends zf.Screen {
	public function new() {
		super();

		final btn = new ui.BasicButton("ui.menuscreen.buttons.start", [80, 20]);
		this.addChild(btn);
		btn.setX(Globals.game.gameWidth, AlignCenter).setY(Globals.game.gameHeight, AlignCenter);

		btn.addOnClickListener("MenuScreen", (e) -> {
			final world = new World(Globals.rules, Globals.currentProfile);
			world.worldState = Globals.rules.newGame();
			this.game.switchScreen(new GameScreen(world));
		});

		final btn = Links.getMastodonBtn();
		btn.setX(10).setY(Globals.game.gameHeight, AnchorBottom, 10);
		this.addChild(btn);

		final btn = Links.getDiscordBtn();
		btn.setX(110).setY(Globals.game.gameHeight, AnchorBottom, 10);
		this.addChild(btn);

		final btn = Links.getTwitterBtn();
		btn.setX(210).setY(Globals.game.gameHeight, AnchorBottom, 10);
		this.addChild(btn);
	}

	override public function update(dt: Float) {}

	override public function render(engine: h3d.Engine) {}

	override public function onEvent(event: hxd.Event) {}

	override public function destroy() {}
}
