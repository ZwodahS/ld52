[
	{
		id: "card:fertiliseall",
		stringId: "cards.fertiliseall",
		render: {
			bitmapId: "card:fertiliseall",
		},
		onPlay: (ctx) -> {
			ctx.increaseNutrientAll(1);
		},
	},
	{
		id: "card:fertiliseadjacent",
		stringId: "cards.fertiliseadjacent",
		render: {
			bitmapId: "card:fertiliseadjacent",
		},
		onPlay: (ctx) -> {
			ctx.increaseNutrientCurrent(2);
			ctx.increaseNutrientAdjacent(2);
		},
	},
	{
		id: "card:fertiliseone",
		stringId: "cards.fertiliseone",
		render: {
			bitmapId: "card:fertiliseone",
		},
		onPlay: (ctx) -> {
			ctx.increaseNutrientCurrent(6);
		},
	},
	{
		id: "card:waterall",
		stringId: "cards.waterall",
		render: {
			bitmapId: "card:waterall",
		},
		onPlay: (ctx) -> {
			ctx.increaseWaterAll(10);
		},
	},
	{
		id: "card:wateradjacent",
		stringId: "cards.wateradjacent",
		render: {
			bitmapId: "card:wateradjacent",
		},
		onPlay: (ctx) -> {
			ctx.increaseWaterCurrent(30);
			ctx.increaseWaterAdjacent(30);
		},
	},
	{
		id: "card:moveforward",
		stringId: "cards.moveforward",
		render: {
			bitmapId: "card:moveforward",
		},
		onPlay: (ctx) -> {
			ctx.moveForward(2);
		},
	},
	{
		id: "card:movebackward",
		stringId: "cards.movebackward",
		render: {
			bitmapId: "card:movebackward",
		},
		onPlay: (ctx) -> {
			ctx.moveBackward(2);
		},
	},
	{
		id: "card:harvestall",
		stringId: "cards.harvestall",
		render: {
			bitmapId: "card:harvestall",
		},
		onPlay: (ctx) -> {
			ctx.triggerHarvest(1);
		},
	},
]
