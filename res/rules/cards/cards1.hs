[
	{
		id: "card:carrot",
		stringId: "crops.carrot",
		render: {
			crop: {
				value: 3, water: 10, growth: 3, wither: 10,
			},
			bitmapId: "crop:carrot",
		},
		onPlay: (ctx) -> {
			ctx.plantCrop("crop:carrot");
		}
	},
	{
		id: "card:watermelon",
		stringId: "crops.watermelon",
		render: {
			crop: {
				value: 6, water: 20, growth: 3, wither: 10,
			},
			bitmapId: "crop:watermelon",
		},
		onPlay: (ctx) -> {
			ctx.plantCrop("crop:watermelon");
		}
	},
	{
		id: "card:peas",
		stringId: "crops.peas",
		render: {
			crop: {
				value: 1, water: 5, growth: 3, wither: 10,
			},
			bitmapId: "crop:peas",
		},
		onPlay: (ctx) -> {
			ctx.plantCrop("crop:peas");
		}
	},
	{
		id: "card:cabbage",
		stringId: "crops.cabbage",
		render: {
			crop: {
				value: 3, water: 5, growth: 5, wither: 10,
			},
			bitmapId: "crop:cabbage",
		},
		onPlay: (ctx) -> {
			ctx.plantCrop("crop:cabbage");
		}
	},
	{
		id: "card:coffeebean",
		stringId: "crops.coffeebean",
		render: {
			crop: {
				value: 0, water: 5, growth: 6, wither: 10,
			},
			bitmapId: "crop:coffeebean",
		},
		onPlay: (ctx) -> {
			ctx.plantCrop("crop:coffeebean");
		}
	},
	{
		id: "card:grape",
		stringId: "crops.grape",
		render: {
			crop: {
				value: 2, water: 10, growth: 4, wither: 10,
			},
			bitmapId: "crop:grape",
		},
		onPlay: (ctx) -> {
			ctx.plantCrop("crop:grape");
		}
	},
	{
		id: "card:potato",
		stringId: "crops.potato",
		render: {
			crop: {
				value: 1, water: 5, growth: 3, wither: 10,
			},
			bitmapId: "crop:potato",
		},
		onPlay: (ctx) -> {
			ctx.plantCrop("crop:potato");
		}
	},
	{
		id: "card:apple",
		stringId: "crops.apple",
		render: {
			crop: {
				value: 2, water: 5, growth: 5, wither: 32,
			},
			bitmapId: "crop:apple",
		},
		onPlay: (ctx) -> {
			ctx.plantCrop("crop:apple");
		}
	},
	{
		id: "card:orange",
		stringId: "crops.orange",
		render: {
			crop: {
				value: 5, water: 10, growth: 4, wither: 32,
			},
			bitmapId: "crop:orange",
		},
		onPlay: (ctx) -> {
			ctx.plantCrop("crop:orange");
		}
	},
	{
		id: "card:sunflower",
		stringId: "crops.sunflower",
		render: {
			crop: {
				value: 6, water: 10, growth: 1, wither: 20,
			},
			bitmapId: "crop:sunflower",
		},
		onPlay: (ctx) -> {
			ctx.plantCrop("crop:sunflower");
		}
	},
	{
		id: "card:moonflower",
		stringId: "crops.moonflower",
		render: {
			crop: {
				value: 6, water: 10, growth: 1, wither: 20,
			},
			bitmapId: "crop:moonflower",
		},
		onPlay: (ctx) -> {
			ctx.plantCrop("crop:moonflower");
		}
	},
	{
		id: "card:camellia",
		stringId: "crops.camellia",
		render: {
			crop: {
				value: 10, water: 10, growth: 1, wither: 4,
			},
			bitmapId: "crop:camellia",
		},
		onPlay: (ctx) -> {
			ctx.plantCrop("crop:camellia");
		}
	},
	{
		id: "card:vines",
		stringId: "crops.vines",
		render: {
			crop: {
				value: 0, water: 2, growth: 10, wither: 1,
			},
			bitmapId: "crop:vines",
		},
		onPlay: (ctx) -> {
			ctx.plantCrop("crop:vines");
		}
	},
	{
		id: "card:grass",
		stringId: "crops.grass",
		render: {
			crop: {
				value: 0, water: 2, growth: 10, wither: 1,
			},
			bitmapId: "crop:grass",
		},
		onPlay: (ctx) -> {
			ctx.plantCrop("crop:grass");
		}
	},
	{
		id: "card:clover",
		stringId: "crops.clover",
		render: {
			crop: {
				value: 0, water: 2, growth: 5, wither: 1,
			},
			bitmapId: "crop:clover",
		},
		onPlay: (ctx) -> {
			ctx.plantCrop("crop:clover");
		}
	},
]
