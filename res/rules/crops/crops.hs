[
	{
		id: "crop:carrot",
		stringId: "crops.carrot",
		render: {
			bitmapId: "crop:carrot",
		},
		getBaseValue: (ctx) -> {
			return 3;
		},
		getRequiredWater: (ctx) -> {
			return 5;
		},
		getGrowthTurn: (ctx) -> {
			return 3;
		},
		getWitherTurn: (ctx) -> {
			return 10;
		},

		onGrow: (ctx) -> {
			if (ctx.depleteWater(10) == false) return;
			ctx.crop.state.timer -= 1;
			if (ctx.depleteNutrient(1) == true) {
				ctx.crop.state.value += 1;
			}
		},
		onMatured: (ctx) -> {},
		onWithered: (ctx) -> {},
	},
	{
		id: "crop:watermelon",
		stringId: "crops.watermelon",
		render: {
			bitmapId: "crop:watermelon",
		},
		getBaseValue: (ctx) -> {
			return 6;
		},
		getRequiredWater: (ctx) -> {
			return 10;
		},
		getGrowthTurn: (ctx) -> {
			return 3;
		},
		getWitherTurn: (ctx) -> {
			return 10;
		},

		onGrow: (ctx) -> {
			if (ctx.depleteWater(20) == false) return;
			ctx.crop.state.timer -= 1;
		},
		onMatured: (ctx) -> {},
		onWithered: (ctx) -> {},
	},
	{
		id: "crop:peas",
		stringId: "crops.peas",
		render: {
			bitmapId: "crop:peas",
		},
		getBaseValue: (ctx) -> {
			return 1;
		},
		getRequiredWater: (ctx) -> {
			return 5;
		},
		getGrowthTurn: (ctx) -> {
			return 3;
		},
		getWitherTurn: (ctx) -> {
			return 10;
		},

		onGrow: (ctx) -> {
			if (ctx.depleteWater(5) == false) return;
			ctx.crop.state.timer -= 1;
		},
		onMatured: (ctx) -> {
			ctx.increaseNutrientAdjacentToPlot(2, ctx.plot);
		},
		onWithered: (ctx) -> {},
	},
	{
		id: "crop:cabbage",
		stringId: "crops.cabbage",
		render: {
			bitmapId: "crop:cabbage",
		},
		getBaseValue: (ctx) -> {
			return 3;
		},
		getRequiredWater: (ctx) -> {
			return 5;
		},
		getGrowthTurn: (ctx) -> {
			return 5;
		},
		getWitherTurn: (ctx) -> {
			return 10;
		},

		onGrow: (ctx) -> {
			if (ctx.depleteWater(5) == false) return;
			ctx.crop.state.timer -= 1;
		},
		onMatured: (ctx) -> {
			var plots = ctx.getAdjacentPlots();
			for (plot in plots) {
				if (plot.plot.crop == null) continue;
				plot.plot.crop.state.value += 1;
			}
		},
		onWithered: (ctx) -> {},
	},
	{
		id: "crop:coffeebean",
		stringId: "crops.coffeebean",
		render: {
			bitmapId: "crop:coffeebean",
		},
		getBaseValue: (ctx) -> {
			return 0;
		},
		getRequiredWater: (ctx) -> {
			return 5;
		},
		getGrowthTurn: (ctx) -> {
			return 6;
		},
		getWitherTurn: (ctx) -> {
			return 10;
		},

		onGrow: (ctx) -> {
			if (ctx.depleteWater(5) == false) return;
			ctx.crop.state.timer -= 1;
			if (ctx.depleteNutrient(1) == true) {
				ctx.crop.state.value += 2;
			}
		},
		onMatured: (ctx) -> {},
		onWithered: (ctx) -> {},
	},
	{
		id: "crop:grape",
		stringId: "crops.grape",
		render: {
			bitmapId: "crop:grape",
		},
		getBaseValue: (ctx) -> {
			return 2;
		},
		getRequiredWater: (ctx) -> {
			return 10;
		},
		getGrowthTurn: (ctx) -> {
			return 4;
		},
		getWitherTurn: (ctx) -> {
			return 10;
		},

		onGrow: (ctx) -> {
			if (ctx.depleteWater(10) == false) return;
			ctx.crop.state.timer -= 1;
			for (plot in ctx.getAdjacentPlots()) {
				if (plot.plot.crop == null) continue;
				if (plot.plot.crop.typeId == "crop:grape") {
					ctx.crop.state.value += 1;
				}
			}
		},
		onMatured: (ctx) -> {},
		onWithered: (ctx) -> {},
	},
	{
		id: "crop:potato",
		stringId: "crops.potato",
		valueMultiplier: 2,
		render: {
			bitmapId: "crop:potato",
		},
		getBaseValue: (ctx) -> {
			return 1;
		},
		getRequiredWater: (ctx) -> {
			return 5;
		},
		getGrowthTurn: (ctx) -> {
			return 3;
		},
		getWitherTurn: (ctx) -> {
			return 10;
		},

		onGrow: (ctx) -> {
			if (ctx.depleteWater(5) == false) return;
			ctx.crop.state.timer -= 1;
		},
		onMatured: (ctx) -> {},
		onWithered: (ctx) -> {},
	},
	{
		id: "crop:apple",
		stringId: "crops.apple",
		persist: true,
		render: {
			bitmapId: "crop:apple",
		},
		getBaseValue: (ctx) -> {
			return 2;
		},
		getRequiredWater: (ctx) -> {
			return 5;
		},
		getGrowthTurn: (ctx) -> {
			return 5;
		},
		getWitherTurn: (ctx) -> {
			return 32;
		},

		onGrow: (ctx) -> {
			if (ctx.depleteWater(5) == false) return;
			ctx.crop.state.timer -= 1;
		},
		onMatured: (ctx) -> {},
		onWithered: (ctx) -> {},
	},
	{
		id: "crop:orange",
		stringId: "crops.orange",
		persist: true,
		render: {
			bitmapId: "crop:orange",
		},
		getBaseValue: (ctx) -> {
			return 5;
		},
		getRequiredWater: (ctx) -> {
			return 10;
		},
		getGrowthTurn: (ctx) -> {
			return 4;
		},
		getWitherTurn: (ctx) -> {
			return 32;
		},

		onGrow: (ctx) -> {
			if (ctx.depleteWaterAndNutrient(5, 1) == false) return;
			ctx.crop.state.timer -= 1;
		},
		onMatured: (ctx) -> {},
		onWithered: (ctx) -> {},
	},
	{
		id: "crop:sunflower",
		stringId: "crops.sunflower",
		render: {
			bitmapId: "crop:sunflower",
		},
		getBaseValue: (ctx) -> {
			return 6;
		},
		getRequiredWater: (ctx) -> {
			return 10;
		},
		getGrowthTurn: (ctx) -> {
			return 1;
		},
		getWitherTurn: (ctx) -> {
			return 10;
		},

		onGrow: (ctx) -> {
			if (ctx.depleteWater(10) == false) return;
			ctx.crop.state.timer -= 1;
		},
		onMatured: (ctx) -> {},
		onWithered: (ctx) -> {},
		canHarvest: (ctx) -> {
			return ctx.hour < 6;
		}
	},
	{
		id: "crop:moonflower",
		stringId: "crops.moonflower",
		render: {
			bitmapId: "crop:moonflower",
		},
		getBaseValue: (ctx) -> {
			return 6;
		},
		getRequiredWater: (ctx) -> {
			return 10;
		},
		getGrowthTurn: (ctx) -> {
			return 1;
		},
		getWitherTurn: (ctx) -> {
			return 10;
		},

		onGrow: (ctx) -> {
			if (ctx.depleteWater(10) == false) return;
			ctx.crop.state.timer -= 1;
		},
		onMatured: (ctx) -> {},
		onWithered: (ctx) -> {},
		canHarvest: (ctx) -> {
			return ctx.hour >= 6;
		}
	},
	{
		id: "crop:camellia",
		stringId: "crops.camellia",
		render: {
			bitmapId: "crop:camellia",
		},
		getBaseValue: (ctx) -> {
			return 10;
		},
		getRequiredWater: (ctx) -> {
			return 10;
		},
		getGrowthTurn: (ctx) -> {
			return 1;
		},
		getWitherTurn: (ctx) -> {
			return 6;
		},

		onGrow: (ctx) -> {
			if (ctx.depleteWater(10) == false) return;
			ctx.crop.state.timer -= 1;
		},
		onMatured: (ctx) -> {},
		onWithered: (ctx) -> {},
	},
	{
		id: "crop:vines",
		stringId: "crops.vines",
		render: {
			bitmapId: "crop:vines",
		},
		getBaseValue: (ctx) -> {
			return 0;
		},
		getRequiredWater: (ctx) -> {
			return 2;
		},
		getGrowthTurn: (ctx) -> {
			return 10;
		},
		getWitherTurn: (ctx) -> {
			return 1;
		},

		onGrow: (ctx) -> {
			if (ctx.depleteWater(2) == false) return;
			ctx.crop.state.timer -= 1;
		},
		onMatured: (ctx) -> {},
		onWithered: (ctx) -> {
			ctx.increaseNutrientAll(2);
		},
	},
	{
		id: "crop:grass",
		stringId: "crops.grass",
		render: {
			bitmapId: "crop:grass",
		},
		getBaseValue: (ctx) -> {
			return 0;
		},
		getRequiredWater: (ctx) -> {
			return 2;
		},
		getGrowthTurn: (ctx) -> {
			return 10;
		},
		getWitherTurn: (ctx) -> {
			return 1;
		},

		onGrow: (ctx) -> {
			if (ctx.depleteWater(2) == false) return;
			ctx.crop.state.timer -= 1;
			if (ctx.depleteNutrient(1) == true) {
				for (plot in ctx.getAdjacentPlots()) {
					if (plot.plot.crop == null) continue;
					plot.plot.crop.state.value += 1;
				}
			}
		},
		onMatured: (ctx) -> {},
		onWithered: (ctx) -> {},
	},
	{
		id: "crop:clover",
		stringId: "crops.clover",
		render: {
			bitmapId: "crop:clover",
		},
		getBaseValue: (ctx) -> {
			return 0;
		},
		getRequiredWater: (ctx) -> {
			return 2;
		},
		getGrowthTurn: (ctx) -> {
			return 5;
		},
		getWitherTurn: (ctx) -> {
			return 1;
		},

		onGrow: (ctx) -> {
			if (ctx.depleteWater(2) == false) return;
			ctx.crop.state.timer -= 1;
		},
		onMatured: (ctx) -> {
			for (plot in ctx.getAdjacentPlots()) {
				if (plot.plot.crop == null) continue;
				plot.plot.crop.state.value += 2;
			}
		},
		onWithered: (ctx) -> {},
	},
]
